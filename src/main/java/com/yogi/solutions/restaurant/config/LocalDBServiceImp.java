package com.yogi.solutions.restaurant.config;

import com.yogi.solutions.restaurant.model.Restaurant;
import org.hibernate.Session;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * @author Krishan Shukla
 * @Date 02/03/2018
 */

@Repository
@Transactional
@ComponentScan(basePackages = "com.yogi.solutions.restaurant")
public class LocalDBServiceImp {

    public  void LocalDBServiceImp() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        Restaurant newRest = new Restaurant();
        newRest.setRestaurantName("dhanana");

         session.saveOrUpdate(newRest);
        //System.out.println(resturant);


        session.getTransaction().commit();
        session.close();

        HibernateUtil.shutdown();
        System.out.println("--onboardingDataIntoDB");

    }

}
