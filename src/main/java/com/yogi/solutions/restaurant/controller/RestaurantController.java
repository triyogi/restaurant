package com.yogi.solutions.restaurant.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yogi.solutions.restaurant.DTO.ResturantDTO;
import com.yogi.solutions.restaurant.model.Restaurant;
import com.yogi.solutions.restaurant.service.ResturantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Krishan Shukla on 20/03/2018.
 */
@RestController
public class RestaurantController {

    @Autowired
    private ResturantService resturantService;

    @RequestMapping(value="/home/all"  , method = RequestMethod.GET)
    public List<Restaurant> getAllResturant() {
        return resturantService.getAllResturant();
    }



        @RequestMapping(value="/home"  , method = RequestMethod.GET)
    public List<Restaurant> getDefaultResturant(){
    return resturantService.getDefaultRestaurant();
}

    @RequestMapping(value="/convertor"  , method = RequestMethod.GET)
    public void getJsonOfResturantDTO(){

        ObjectMapper objectMapper = new ObjectMapper();
        ResturantDTO resturantDTO = new ResturantDTO();
        resturantDTO.setAddress("330 Guraha");
        resturantDTO.setContactNumber("576869783");
        resturantDTO.setCreateBy("Buby");
        resturantDTO.setEmailAddress("Buby@savi.com");
        resturantDTO.setOpeningTime("12 -11PM");
        resturantDTO.setRestaurantName("Tripathi Dhaba");
        resturantDTO.setSpeciality("veg special khana");

        try {
            System.out.println(objectMapper.writeValueAsString(resturantDTO));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }

    @RequestMapping(value="/home/createresturant" , method = RequestMethod.POST)
    public Restaurant createResturant(@RequestBody ResturantDTO returantDTO){
        return resturantService.createResturant(returantDTO);
    }
}
