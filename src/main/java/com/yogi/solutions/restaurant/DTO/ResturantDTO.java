package com.yogi.solutions.restaurant.DTO;

import java.util.Arrays;

/**
 * Created by Krishan Shukla on 26/03/2018.
 */
public class ResturantDTO {

    private String restaurantName;
    private String contactNumber;
    private String address;
    private String openingTime;
    private String speciality;
    private byte[] logo;
    private String createBy;
    private String emailAddress;


    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOpeningTime() {
        return openingTime;
    }

    public void setOpeningTime(String openingTime) {
        this.openingTime = openingTime;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public byte[] getLogo() {
        return logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Override
    public String toString() {
        return "ResturantDTO{" +
                "restaurantName='" + restaurantName + '\'' +
                ", contactNumber='" + contactNumber + '\'' +
                ", address='" + address + '\'' +
                ", openingTime='" + openingTime + '\'' +
                ", speciality='" + speciality + '\'' +
                ", logo=" + Arrays.toString(logo) +
                ", createBy='" + createBy + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                '}';
    }
}
