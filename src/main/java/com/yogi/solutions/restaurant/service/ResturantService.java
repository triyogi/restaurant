package com.yogi.solutions.restaurant.service;

import com.yogi.solutions.restaurant.DTO.ResturantDTO;
import com.yogi.solutions.restaurant.model.Restaurant;
import com.yogi.solutions.restaurant.repository.RestaurantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Krishan Shukla on 20/03/2018.
 */
@Service
public class ResturantService {

    @Autowired
    private RestaurantRepository restaurantRepository;

    public List<Restaurant> getAllResturant(){
        return restaurantRepository.getAll();
    }

    public List<Restaurant> getDefaultRestaurant(){
        List<Restaurant> listOfResturant = new ArrayList<>();
       Restaurant rest = new Restaurant();
        rest.setRestaurantName("TasteOfIndia");
        listOfResturant.add(rest);

        Restaurant rest2 = new Restaurant();
        rest2.setRestaurantName("SarvanaBhavan");
        rest2.setAddress("IllfordRoad");
        listOfResturant.add(rest2);
        return  listOfResturant;
    }

    public Restaurant createResturant(ResturantDTO resturantDTO) {
        return restaurantRepository.saveInDB(resturantDTO);
    }
}
