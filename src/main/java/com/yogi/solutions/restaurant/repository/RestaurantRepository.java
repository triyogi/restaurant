package com.yogi.solutions.restaurant.repository;

import com.yogi.solutions.restaurant.DTO.ResturantDTO;
import com.yogi.solutions.restaurant.config.HibernateUtil;
import com.yogi.solutions.restaurant.model.Restaurant;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Krishan Shukla on 27/03/2018.
 */
@Repository
@Transactional
public class RestaurantRepository {

    public Restaurant saveInDB(ResturantDTO resturantDTO){
        Session sessionFactory =HibernateUtil.getSessionFactory().openSession();
        Restaurant rest = new Restaurant();
        rest.setRestaurantName(resturantDTO.getRestaurantName());
        rest.setAddress(resturantDTO.getAddress());
        rest.setContactNumber(resturantDTO.getContactNumber());
        rest.setOpeningTime(resturantDTO.getOpeningTime());
        rest.setSpeciality(resturantDTO.getSpeciality());

        sessionFactory.beginTransaction();
        sessionFactory.save(rest);
        sessionFactory.getTransaction().commit();
        sessionFactory.close();
      //  HibernateUtil.shutdown();

        return rest;
    }
    public List<Restaurant> getAll(){
        Session sessionFactory =HibernateUtil.getSessionFactory().openSession();
        sessionFactory.beginTransaction();
        List<Restaurant> myAllRestaurant = sessionFactory.createQuery("from Restaurant").list();
        sessionFactory.getTransaction().commit();
        sessionFactory.close();
     //   HibernateUtil.shutdown();

        return myAllRestaurant;
    }

}
